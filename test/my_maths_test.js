const assert = require('assert');
const maths = require('../my_maths.js');

describe('Maths', () => {
	it('add(1, 1) should return 2', () => {
		const res = maths.add(1, 1);
		assert.equal(res, 2);
	});

	it('sub(1, 1) should return 0', () => {
		const res = maths.sub(1, 1);
		assert.equal(res, 0);
	});
});